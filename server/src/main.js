const express = require("express");
const app = express();
const port = 8000;
const bodyParser = require("body-parser");
const cors = require("cors");

let count = 0;

app.use(cors())

app.get("/count", bodyParser.json(), (req, res) => {
  count++;
  res.json({ count: count });
});

app.get("/get/count", bodyParser.json(), (req, res) => {
  res.json({ count: count });
});

app.listen(port, () => {
  console.log(`Server on port ${port}`);
});
