import axios from "axios";

let count = 0;

async function counter() {
  let result = await axios.get("http://localhost:8000/count");
  console.log(result.data);
}

async function getCounter() {
  let result = await axios.get("http://localhost:8000/get/count");
  console.log(result.data);
  count = result.data.count;
}

function App() {
  return (
    <div>
      {count}
      <br />
      <button onClick={counter}>Counter</button>
      <br />
      <button onClick={getCounter}>Get Counter</button>
    </div>
  );
}

export default App;
